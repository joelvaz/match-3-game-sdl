//
//  TextureManager.hpp
//  Match-3-Game-SDL
//
//  Created by Joel Vaz on 06/07/2020.
//  Copyright © 2020 Joel Vaz. All rights reserved.
//

#ifndef TextureManager_hpp
#define TextureManager_hpp

#include "Game.hpp"

class TextureManager
{
public:
    static SDL_Texture* LoadTexture(const char* fileName);
    static void Draw(SDL_Texture* tex, SDL_Rect src, SDL_Rect dest);
};

#endif /* TextureManager_hpp */
