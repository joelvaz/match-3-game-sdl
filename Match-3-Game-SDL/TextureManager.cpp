//
//  TextureManager.cpp
//  Match-3-Game-SDL
//
//  Created by Joel Vaz on 06/07/2020.
//  Copyright © 2020 Joel Vaz. All rights reserved.
//

#include "TextureManager.hpp"

SDL_Texture* TextureManager::LoadTexture(const char* texture)
{
    SDL_Surface* tempSurface= IMG_Load(texture);
    SDL_Texture* tex= SDL_CreateTextureFromSurface(Game::renderer, tempSurface);
    SDL_FreeSurface(tempSurface);
    
    return tex;
}

void TextureManager::Draw(SDL_Texture* tex, SDL_Rect src, SDL_Rect dest){
    
    SDL_RenderCopy(Game::renderer, tex, &src, &dest);
}
