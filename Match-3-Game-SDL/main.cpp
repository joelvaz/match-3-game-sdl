//
//  main.cpp
//  Match-3-Game-SDL
//
//  Created by Joel Vaz on 06/07/2020.
//  Copyright © 2020 Joel Vaz. All rights reserved.
//

#include "Game.hpp"

Game* game= nullptr;

int main(int argc, const char * argv[]) {
    const int FPS= 60;
    const int frameDelay= 1000/FPS;
    Uint32 frameStart;
    int frameTime;
    
    game= new Game();
    game->init(WINDOW_TITLE, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, WINDOW_WIDTH, WINDOW_HEIGHT, FLAG_FULLSCREEN);
    
    //Game Loop
    while(game->running()){
        frameStart= SDL_GetTicks();
        
        game->handleEvents();
        game->update();
        game->render();
        
        frameTime= SDL_GetTicks() - frameStart;
        if(frameDelay > frameTime) { SDL_Delay(frameDelay - frameTime); }
    }
    game->clean();
    return EXIT_SUCCESS;
}
