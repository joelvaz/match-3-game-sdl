//
//  Game.cpp
//  Match-3-Game-SDL
//
//  Created by Joel Vaz on 06/07/2020.
//  Copyright © 2020 Joel Vaz. All rights reserved.
//

#include "Game.hpp"

SDL_Renderer *Game::renderer= nullptr;
SDL_Event Game::event;

Game::Game() {}
Game::~Game() {}

void Game::init(const char* title, int xPos, int yPos, int width, int height, bool fullscreen){
    int flags= 0;
    
    if(fullscreen)
    {
        flags= SDL_WINDOW_FULLSCREEN;
    }
    if( SDL_Init(SDL_INIT_EVERYTHING) == 0){
        std::cout << "Subsytems Initialized!..." << std::endl;
        
        window= SDL_CreateWindow(title, xPos, yPos, width, height, flags);
        if(window){
            std::cout << "Window Created!" << std::endl;
        }
        
        renderer= SDL_CreateRenderer(window, -1, 0);
        if(renderer){
            SDL_SetRenderDrawColor(renderer, 255, 204, 153, 255);
            std::cout << "Renderer Created!" << std::endl;
        }
        
        isRunning= true;
    }else{ isRunning= false; }
}

void Game::handleEvents(){
    SDL_PollEvent(&event);
    
    switch(event.type) {
        case SDL_QUIT:
            isRunning= false;
            break;
        default:
            break;
    }
}

void Game::update(){
    
}

void Game::render(){
    SDL_RenderClear(renderer);
    SDL_RenderPresent(renderer);
}

void Game::clean(){
    SDL_DestroyWindow(window);
    SDL_DestroyRenderer(renderer);
    SDL_Quit();
    std::cout << "Game Cleaned!" << std::endl; 
}
