//
//  Vector2D.hpp
//  Match-3-Game-SDL
//
//  Created by Joel Vaz on 06/07/2020.
//  Copyright © 2020 Joel Vaz. All rights reserved.
//

#ifndef Vector2D_hpp
#define Vector2D_hpp

#include <iostream>

class Vector2D
{
public:
    float x;
    float y;
    
    Vector2D();
    Vector2D(float x, float y);
    
    Vector2D& Add(const Vector2D& vec);
    Vector2D& Subtract(const Vector2D& vec);
    Vector2D& Multiply(const Vector2D& vec);
    Vector2D& Divide(const Vector2D& vec);
    
    friend Vector2D& operator+(Vector2D& v1, const Vector2D& v2);
    friend Vector2D& operator-(Vector2D& v1, const Vector2D& v2);
    friend Vector2D& operator*(Vector2D& v1, const Vector2D& v2);
    friend Vector2D& operator/(Vector2D& v1, const Vector2D& v2);
    
    Vector2D& operator+=(const Vector2D& vec);
    Vector2D& operator-=(const Vector2D& vec);
    Vector2D& operator*=(const Vector2D& vec);
    Vector2D& operator/=(const Vector2D& vec);
    
    Vector2D& operator*(const int& i);
    Vector2D& Zero();
    
    friend std::ostream& operator<<(std::ostream& stream, const Vector2D& vec); 
};

#endif /* Vector2D_hpp */
