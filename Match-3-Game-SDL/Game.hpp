//
//  Game.hpp
//  Match-3-Game-SDL
//
//  Created by Joel Vaz on 06/07/2020.
//  Copyright © 2020 Joel Vaz. All rights reserved.
//

#ifndef Game_hpp
#define Game_hpp

#include <iostream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

const int WINDOW_WIDTH= 800, WINDOW_HEIGHT= 640;
const bool FLAG_FULLSCREEN= false;
const char WINDOW_TITLE[]= "Match-3 Game";

class Game{
public:
    Game();
    ~Game();
    
    static SDL_Renderer* renderer;
    static SDL_Event event;
    
    void init(const char* title, int xPos, int yPos, int width, int height, bool fullscreen);
    void handleEvents();
    void update();
    void render();
    void clean();
    
    bool running() { return isRunning; }
    
private:
    bool isRunning;
    SDL_Window* window; 
};

#endif /* Game_hpp */
