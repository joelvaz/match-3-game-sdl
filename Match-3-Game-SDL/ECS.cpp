//
//  ECS.cpp
//  Match-3-Game-SDL
//
//  Created by Joel Vaz on 07/07/2020.
//  Copyright © 2020 Joel Vaz. All rights reserved.
//

#include "ECS.hpp"

void Entity::addGroup(Group mGroup) {
    groupBitSet[mGroup] = true;
    manager.AddToGroup(this, mGroup);
}
